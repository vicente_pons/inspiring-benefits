.DEFAULT_GOAL := start

.PHONY: install
install:: ##Install application dependencies
	@docker-compose run --rm app rails db:setup

.PHONY: start
start:: ##Starts application for development
	@docker-compose up

.PHONY: stop
stop:: ##Stops and removes containers, images... created by 'start'
	@docker-compose down --volumes --rmi local

.PHONY: test
test:: ##Run all test
	@docker-compose run --rm app rails test:system
	@docker-compose run --rm app rails test
