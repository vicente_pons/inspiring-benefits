class CreateFacts < ActiveRecord::Migration[5.2]
  def change
    create_table :facts do |t|
      t.string :category
      t.string :icon_url
      t.string :guid
      t.string :url
      t.text :value

      t.timestamps
    end
  end
end
