class SearchResultsController < ApplicationController
  def index
    @search_results = facts_finder_service.call(search_params)
    @categories = facts_categories_service.call
  end

  private

  def facts_finder_service
    @facts_finder_service ||= FactsFinderService.new(ApiConsumer)
  end

  def facts_categories_service
    @facts_categories_service ||= FactsCategoriesService.new(ApiConsumer)
  end

  def search_params
    params.permit(:query, :random)
  end
end
