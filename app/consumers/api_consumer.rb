require "net/http"

class ApiConsumer
  class << self
    API_BASE_URL = "https://api.chucknorris.io/jokes"

    def call(endpoint)
      uri = URI(API_BASE_URL + endpoint)

      response = request(uri)

      json(response.body)
    end

    private

    def request(uri)
      Net::HTTP.start(uri.host, uri.port, use_ssl: true) do |http|
        request = Net::HTTP::Get.new(uri)
        http.request(request)
      end
    end

    def json(body)
      JSON.parse(body, symbolize_names: true)
    rescue
      {}
    end
  end
end
