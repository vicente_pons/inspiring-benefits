class FactsFinderService
  def initialize(consumer)
    @consumer = consumer
  end

  def call(params)
    return Fact.none if params.empty? || params.values.first.blank?

    facts = @consumer.call("/search?query=#{params[:query]}") if params[:query].present?

    if params[:random].present?
      if params[:category].present?
        facts = @consumer.call("/random?category=#{params[:category]}") 
      else
        facts = @consumer.call("/random") 
      end
    end

    results(facts)
  end

  private

  def results(facts)
    unless facts[:result].nil?
      facts[:result].map do |fact_attrs|
        build(fact_attrs)
      end
    else
      [build(facts)]
    end
  end

  def build(fact)
    Fact.new(fact.merge(guid: fact[:id]))
  end
end
