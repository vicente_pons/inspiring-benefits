class FactsCategoriesService
  def initialize(consumer)
    @consumer = consumer
  end

  def call
    @consumer.call("/categories")
  end
end
