# Inspiring Benefits

##Getting Started

In order to start it, we must install docker and docker compose. To run the project we can do the following:

    $ make install
    $ make start

For running all test:

    $ make test
