require "application_system_test_case"

class SearchFactsTest < ApplicationSystemTestCase
  test "visiting the index without login" do
    visit "/"

    assert_selector "[data-test=search-results-title]"
  end

  test "searching by words" do
    words = "god"

    visit "/"

    fill_in("query", with: words)
    click_button("search-button")

    assert_text "[data-test=search-results]", words
  end

  test "random" do
    visit "/"

    click_button("random-button")

    assert_selector "[data-test=search-results]"
  end

  test "random by category" do
    visit "/"

    category = find("[data-test=categories] > option:nth-of-type(2)").text

		select(category, from: "categories")
    click_button("random-button")

    assert_text "[data-test=search-results]", category
  end
end
