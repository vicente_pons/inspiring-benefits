require "test_helper"

class FactsFinderServiceTest < ActiveSupport::TestCase
  setup do
    @expected_fact = Fact.new({
      category: nil,
      icon_url: "https://assets.chucknorris.host/img/avatar/chuck-norris.png",
      id: "y-bNIK9GScaNSMWewihm0w",
      url: "https://api.chucknorris.io/jokes/y-bNIK9GScaNSMWewihm0w",
      value: "Who knows what evil lurks in the hearts of men? Goddamn Chuck Norris, that's who."
    })

  end

  test "no params" do
    consumer = Minitest::Mock.new

    facts = FactsFinderService.new(consumer).call({})

    assert_equal facts, Fact.none
    assert consumer.verify
  end

  test "find facts by query" do
    query = "words"
    api_consumer_results = {
      total: 1, 
      result: [@expected_fact.attributes]
    }

    consumer = Minitest::Mock.new
    consumer.expect :call, api_consumer_results, ["/search?query=#{query}"]

    facts = FactsFinderService.new(consumer).call({ query: query })

    assert_equal facts, [@expected_fact]
    assert consumer.verify
  end

  test "find facts randomly" do
    api_consumer_results = @expected_fact.attributes

    consumer = Minitest::Mock.new
    consumer.expect :call, api_consumer_results, ["/random"]

    facts = FactsFinderService.new(consumer).call({ random: true })

    assert_equal facts, [@expected_fact]
    assert consumer.verify
  end

  test "find facts by category" do
		category = "movies"
    api_consumer_results = @expected_fact.attributes

    consumer = Minitest::Mock.new
    consumer.expect :call, api_consumer_results, ["/random?category=#{category}"]

    facts = FactsFinderService.new(consumer).call({ random: true, category: category })

    assert_equal facts, [@expected_fact]
    assert consumer.verify
  end
end
