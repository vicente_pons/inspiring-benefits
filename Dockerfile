FROM ruby:2.6.3
LABEL maintainer="vicentepons.rb@gmail.com"

ENV APP_PATH /usr/local/app
WORKDIR $APP_PATH

RUN apt-get update && apt-get install -y nodejs

RUN mkdir /tmp/phantomjs \
 && curl -L https://bitbucket.org/ariya/phantomjs/downloads/phantomjs-2.1.1-linux-x86_64.tar.bz2 \
  | tar -xj --strip-components=1 -C /tmp/phantomjs \
 && mv /tmp/phantomjs/bin/phantomjs /usr/local/bin

COPY Gemfile Gemfile.lock $APP_PATH/
RUN bundle install

COPY . $APP_PATH

EXPOSE 3000

CMD ["rails", "server", "-b", "0.0.0.0"]
